import firebase from './firebase';

const firebaseConfig = {
    apiKey: "AIzaSyAWzEwmgGiFbftofaREknuTP1tZHmH4dC8",
    authDomain: "imessage-clone-50531.firebaseapp.com",
    projectId: "imessage-clone-50531",
    storageBucket: "imessage-clone-50531.appspot.com",
    messagingSenderId: "344362454298",
    appId: "1:344362454298:web:1c496c0a5ea4910d46b0a3",
    measurementId: "G-DLJMTRB2JK"
  };

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export { auth, provider };
export default db;