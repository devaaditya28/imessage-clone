import { Button } from "@material-ui/core";
import React from "react";
import "./Login.css";

function Login() {
    return (
        <div className="login">
            <div className="login_logo">
                <img src="https://icon-library.com/images/icon-chat/icon-chat-16.jpg" alt="" />
                <h1>iMessage</h1>
            </div>

            <Button>Sign In</Button>
        </div>
    );
}

export default Login;